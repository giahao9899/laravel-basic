<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use App\Category;
use App\Product;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
    ];
});

$factory->define(Category::class, function (Faker $faker) {
    return [
        'name' => $faker->text(20),
        'slug' => $faker->slug
    ];
});

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->text(20),
        'slug' => $faker->slug,
        'image' => $faker->imageUrl(200),
        'desc' => $faker->text(200),
        'price' => $faker->numberBetween(1000, 1000000),
        'priceSale' => $faker->numberBetween(1000, 1000000),
        'category_id' => $faker->randomElement(Category::all()->pluck('id')->toArray())
    ];
});