# Bài tập Trainee Colombo 2019

## Laravel basic

Thực hiện bởi [Nguyễn Gia Hào](https://gitlab.com/giahao9899)



## Hướng dẫn cài đặt

- Clone project
- Rename `.env.example` file to `.env` inside your project root and fill the database information.
- Create Database
- Run `composer install`
- Run `php artisan key:generate`
- Run `php artisan migrate`
- Run `php artisan db:seed` to run seeders
- Run php artisan serve