@extends('admin.login.layout')

@section('content')

    <!-- Nested Row within Card Body -->
    <div class="row">
        <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
        <div class="col-lg-7">
            <div class="p-5">
                <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
                </div>
                <form class="user" action="{{ route('admin.register.postRegister') }}" method="post">
                  @csrf
                    <div class="form-group">
                        <input type="text" class="form-control form-control-user" name="name" id="name" placeholder="First Name">
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control form-control-user" name="email" id="email" placeholder="Email Address">
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <input type="password" class="form-control form-control-user" name="password" id="password" placeholder="Password">
                        </div>
                        <div class="col-sm-6">
                            <input type="password" class="form-control form-control-user" name="password_confirmation" id="repeatPassword" placeholder="Repeat Password">
                        </div>
                    </div>
                    <button class="btn btn-primary btn-user btn-block">
                        Register Account
                    </button>
                </form>
                <hr>
                <div class="text-center">
                    <a class="small" href="#">Forgot Password?</a>
                </div>
                <div class="text-center">
                    <a class="small" href="{{ route('admin.login.getLogin') }}">Already have an account? Login!</a>
                </div>
            </div>
        </div>
    </div>
@endsection