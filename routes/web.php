<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Admin',  'prefix' => 'admin'], function () {
    Route::group(['prefix' => 'login', 'middleware' => 'guest'], function() {

        Route::get('/' , 'LoginController@getLogin')->name('admin.login.getLogin');
        Route::post('/' , 'LoginController@postLogin')->name('admin.login.postLogin');
    });
});

Route::get('admin/register', 'Auth\RegisterController@show')->name('admin.register.show');
Route::post('admin/register', 'Auth\RegisterController@register')->name('admin.register.postRegister');

Route::get('admin/forgotPassword', 'Auth\ForgotPasswordController@show')->name('admin.forgotPassword.show');

Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => 'checklogin'], function () {
   Route::get('/', function () {
       return view('admin.index');
})->name('admin');

   Route::get('logout', 'LoginController@logout')->name('logout');

   Route::group(['prefix' => 'category'], function () {
       Route::get('/', 'CategoryController@index')->name('admin.category');
       Route::post('/', 'CategoryController@postCreateCategory')->name('admin.category.postCreate');

       Route::get('/update/{id}', 'CategoryController@getUpdate')->name('admin.category.getUpdate');
       Route::post('/update/{id}', 'CategoryController@postUpdate')->name('admin.category.postUpdate');

       Route::get('/delete/{id}', 'CategoryController@destroy')->name('admin.category.destroy');
   });

    Route::group(['prefix' => 'product'], function () {
        Route::get('/', 'ProductController@index')->name('admin.product');

        Route::get('/create', 'ProductController@getCreateProduct')->name('admin.product.getCreate');
        Route::post('/create', 'ProductController@postCreateProduct')->name('admin.product.postCreate');

        Route::get('/update/{id}', 'ProductController@getUpdate')->name('admin.product.getUpdate');
        Route::post('/update/{id}', 'ProductController@postUpdate')->name('admin.product.postUpdate');

        Route::get('/delete/{id}', 'ProductController@destroy')->name('admin.product.destroy');

        Route::get('/active/{id}', 'ProductController@getActive')->name('admin.product.getActive');
    });


});

Route::group(['namespace' => 'User'], function () {
    Route::get('/', 'UserController@index')->name('user');

    Route::get('/{slug}.html', 'UserController@getProduct')->name('user.getProduct');
    Route::get('/category/{slug}', 'UserController@getProductOfCategory')->name('user.getProductOfCategory');

    Route::get('/ajax/category/{slug}', 'UserController@AjaxGetProduct')->name('user.AjaxGetProduct');
});
