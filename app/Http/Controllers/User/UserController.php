<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Product;

class UserController extends Controller
{
    public function index() {

        $categories = Category::all();
        $productOfCategory = $categories[0]->product()
                                            ->where('active', 1)
                                            ->get();
        $products = Product::where('active', 1)->take(10)->get();

        return view('user.index', [
            'categories' => $categories,
            'productOfCategory' => $productOfCategory,
            'products' => $products
        ]);
    }


    public function getProduct($slug) {

        $categories = Category::all();
        $product = Product::where('slug', $slug)->firstOrFail();

        return view('user.product', [
            'categories' => $categories,
            'product' => $product
        ]);
    }

    public function getProductOfCategory($slug) {

        $categories = Category::all();
        $category = Category::where('slug', $slug)->firstOrFail();
        $categoryName = $category->name;

        $products = $category->product()->where('active', 1)->paginate(9);


        return view('user.category', [
            'categories' => $categories,
            'products' => $products,
            'categoryName' => $categoryName
        ]);
    }

    public function AjaxGetProduct($slug) {
        $category = Category::where('slug', $slug)->first();
        $products = $category->product()->where('active', 1)->get();

        return json_encode($products);
    }


}
