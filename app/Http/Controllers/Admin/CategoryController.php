<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CategoryRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use Illuminate\Support\Str;

class CategoryController extends Controller
{

    public function index()
    {
        $categories = Category::all();

        return view('admin.category.index', [
            'categories' => $categories
        ]);
    }


    public function postCreateCategory(CategoryRequest $request)
    {
        $category = new Category();

        $category->name = $request->name;

        $category->slug = Str::slug($request->name, '-');

        $category->save();

        return redirect()->back()->with('success', 'Đã thêm 1 danh mục');
    }


    public function getUpdate($id)
    {
        $category = Category::findOrFail($id);

        return view('admin.category.update', [
            'category' => $category
        ]);
    }


    public function postUpdate(CategoryRequest $request, $id)
    {
        $category = Category::findOrFail($id);

        $category->name = $request->name;

        $category->slug = Str::slug($request->name, '-');

        $category->save();

        return redirect('admin/category')->with('success', 'Đã update 1 danh mục');
    }


    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        $products = $category->product();
        if(count($products->get()) > 0) {
            foreach ($products as $product) {
                @unlink($product->image);
            }
            $products->delete();
        }
        $category->delete();

        return redirect()->back()->with('success', 'Đã xóa 1 danh mục');
    }
}
