<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use App\Category;
use App\Product;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::with('category')->get();

        return view('admin.product.index', [
            'products' => $products
        ]);
    }

    public function getCreateProduct() {
        $categories = Category::all();

        return view('admin.product.create', [
            'categories' => $categories
        ]);
    }

    public function postCreateProduct(ProductRequest $request)
    {
        $product = new Product();

        $product->name = $request->name;

        if(Product::where('name', $request->name)->exists()) {
            $product->slug = Str::slug($request->name, '-') . '-' . Str::random(10);
        }
        else {
            $product->slug = Str::slug($request->name, '-');
        }

        $product->price = $request->price;

        if($request->priceSale == null || $request->priceSale > $request->price) {
            $product->priceSale = $request->price;
        }else {
            $product->priceSale = $request->priceSale;
        }

        $product->desc = $request->desc;

        $product->active = 1;

        $product->category_id = $request->category_id;

        if($request->hasFile('image')) {
            //Upload image
            $image = $request->image;

            $image_new_name = time().$image->getClientOriginalName();

            $image->move('upload/product', $image_new_name);
            $product->image = 'upload/product/' . $image_new_name;
        }
        else {
            $product->image = '?';
        }
        $product->save();

        return redirect('admin/product')->with('success', 'Bạn đã thêm 1 sản phẩm mới');
    }


    public function getUpdate($id)
    {
        $product = Product::findOrFail($id);
        $categories = Category::all();

        return view('admin.product.update', [
            'categories' => $categories,
            'product' => $product
        ]);
    }


    public function postUpdate(ProductRequest $request, $id)
    {
        $product = Product::findOrFail($id);

        $product->name = $request->name;

        if(Product::where('name', $request->name)->exists()) {
            $product->slug = Str::slug($request->name, '-') . '-' . Str::random(10);
        }
        else {
            $product->slug = Str::slug($request->name, '-');
        }

        $product->price = $request->price;

        if($request->priceSale == null || $request->priceSale > $request->price) {
            $product->priceSale = $request->price;
        }
        else {
            $product->priceSale = $request->priceSale;
        }

        $product->desc = $request->desc;

        $product->category_id = $request->category_id;

        if($request->hasFile('image')) {
            $image = $request->image;

            $image_new_name = time() . $image->getClientOriginalName();

            $image->move('upload/product', $image_new_name);

            unlink($product->image);
            $product->image = 'uploads/products/' . $image_new_name;
        }

        $product->save();

        return redirect('admin/product')->with('success', 'Bạn đã sửa 1 sản phẩm');
    }


    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        unlink($product->image);
        $product->delete();

        return redirect()->back()->with('success', 'Đã xóa thành công');
    }

    public function getActive($id) {
        $product = Product::findOrFail($id);
        if($product->active == 1) {
            $product->active = 0;
        }
        else {
            $product->active = 1;
        }
        $product->save();
        return redirect()->back();
    }
}
