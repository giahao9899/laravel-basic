<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:categories,name'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Tên danh muc không được để trống',
            'name.unique' => 'Đã tồn tại danh mục'
        ];
    }

    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(redirect()
            ->back()
            ->withInput()
            ->withErrors($validator)
        );
    }
}
