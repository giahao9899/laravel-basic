<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;



class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'price' => 'required',
            'desc' => 'required',
            'image' => 'image',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Tên sản phẩm không được để trống',
            'price.required'  => 'Giá sản phẩm không được để trống',
            'desc.required' => 'Thêm mô tả sản phẩm',
            'image.image' => 'Định dạng hình ảnh không đúng'
        ];
    }

    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(redirect()
            ->back()
            ->withInput()
            ->withErrors($validator)
        );
    }
}
